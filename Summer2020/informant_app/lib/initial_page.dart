import 'package:flutter/material.dart';
import 'package:informant_app/util/strings.dart';
import 'routes/router.gr.dart';

class InitialPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/mhbs.png"),
            SizedBox(
              height: 10,
            ),
            Text(
              appName,
              style: TextStyle(fontSize: 32, color: Colors.white, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 60,
            ),
            RaisedButton(
              color: Colors.white,
              onPressed: (){
                Router.navigator.pushNamed(Router.getQRPage);
              },
              child: Text("Connect", style: TextStyle(color: Colors.blue),),

            )
          ],
        ),
      ),
    );
  }
}
