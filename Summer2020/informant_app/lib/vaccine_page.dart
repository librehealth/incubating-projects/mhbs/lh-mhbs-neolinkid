import 'package:flutter/material.dart';

import 'custom_app_bar.dart';
import 'routes/router.gr.dart';
import 'util/strings.dart';
import 'util/style.dart';

class VaccinePage extends StatefulWidget {
  @override
  _VaccinePageState createState() => _VaccinePageState();
}

class _VaccinePageState extends State<VaccinePage> {
  String vaccineName;
  String administrationSite;
  String location;
  TextEditingController expirationDate = TextEditingController();
  TextEditingController dateGiven = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Text(
                "Vaccination Credential",
                style: TextStyle(color: Colors.blue, fontSize: 28),
              ),
              Text(
                enterDetailsBelow,
                style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              SizedBox(
                height: 20,
              ),
              /*listOfFields(widget.credentialType),*/
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Vaccine Name"),
                  SizedBox(
                    height: 4,
                  ),
                  DropdownButtonFormField(
                    onChanged: (newValue){
                      setState(() => vaccineName = newValue);
                    } ,
                    items: <String>['BCG', 'HepB', 'OPV','Rotavirus','Measles','DPT','Yellow Fever'].map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: textFormFieldDecoration,
                    value: vaccineName,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Lot Number"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    onTap: (){

                    },
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  Text("Expiration Date"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    controller: expirationDate,
                    decoration: textFormFieldDecoration,
                    //initialValue: birthDate,
                    onTap: () async {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      var date = await showDatePicker(
                          context: context,
                          initialDate:DateTime.now(),
                          firstDate:DateTime.now(),
                          lastDate: DateTime(2100) );
                      FocusScope.of(context).requestFocus(new FocusNode());
                      setState(() {
                        expirationDate.text = "${date.day}/${date.month}/${date.year}";
                      });

                    },
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  Text("Date given"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    controller: dateGiven,
                    decoration: textFormFieldDecoration,
                    //initialValue: birthDate,
                    onTap: () async {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      var date = await showDatePicker(
                          context: context,
                          initialDate:DateTime.now(),
                          firstDate:DateTime(1980),
                          lastDate:DateTime.now());
                      FocusScope.of(context).requestFocus(new FocusNode());
                      setState(() {
                        dateGiven.text = "${date.day}/${date.month}/${date.year}";
                      });

                    },
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  Text("Administration Site"),
                  SizedBox(
                    height: 4,
                  ),
                  DropdownButtonFormField(
                    onChanged: (newValue){
                      setState(() => administrationSite = newValue);
                    } ,
                    items: <String>['L arm', 'Other'].map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: textFormFieldDecoration,
                    value: administrationSite,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  Text("Location"),
                  SizedBox(
                    height: 4,
                  ),
                  DropdownButtonFormField(
                    onChanged: (newValue){
                      setState(() => location = newValue);
                    } ,
                    items: <String>['Moi University Hospital'].map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: textFormFieldDecoration,
                    value: location,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                ],
              ),
              RaisedButton(
                color: Colors.blue,
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      // return object of type Dialog
                      return AlertDialog(
                        title: Text("Confirmation", style: TextStyle(color: Colors.black),),
                        content: Text("Are the following details correct?",style: TextStyle(color: Colors.black),),
                        actions: <Widget>[
                          // usually buttons at the bottom of the dialog
                          FlatButton(
                            child: Text("Confirm",style: TextStyle(color: Colors.blue),),
                            onPressed: () {
                              Navigator.pop(context);
                              Router.navigator.pushNamed(
                                Router.sendingCredentialPage,
                                arguments: SendingCredentialPageArguments(
                                  credentialName: "Vaccine",
                                ),
                              );
                            },
                          ),
                          FlatButton(
                            child: Text("Edit",style: TextStyle(color: Colors.black),),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                color: Colors.white,
                onPressed: () {},
                child: Text(
                  "Cancel",
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
