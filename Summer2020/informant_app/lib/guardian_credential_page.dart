import 'package:flutter/material.dart';
import 'package:informant_app/custom_app_bar.dart';
import 'package:informant_app/util/strings.dart';

import 'routes/router.gr.dart';
import 'util/style.dart';

class GuardianCredentialPage extends StatefulWidget {
  const GuardianCredentialPage({Key key}) : super(key: key);

  @override
  _GuardianCredentialPageState createState() => _GuardianCredentialPageState();
}

class _GuardianCredentialPageState extends State<GuardianCredentialPage> {
  TextEditingController dateCtl = TextEditingController();
  String subCounty;

  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Text(
                "Guardian ID Credential",
                style: TextStyle(color: Colors.blue, fontSize: 28),
              ),
              Text(
                enterDetailsBelow,
                style: TextStyle(color: Colors.blue, fontSize: 20),
              ),
              SizedBox(
                height: 20,
              ),
              /*listOfFields(widget.credentialType),*/
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("First Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Middle Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    onTap: () {},
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Father's Name"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    decoration: textFormFieldDecoration,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Sub-County"),
                  SizedBox(
                    height: 4,
                  ),
                  DropdownButtonFormField(
                    onChanged: (newValue) {
                      setState(() => subCounty = newValue);
                    },
                    items: <String>['Turbo', 'Kessen', 'Moiben', 'Kapseret','Soy','Ainabkoi','Eldoret']
                        .map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    decoration: textFormFieldDecoration,
                    value: subCounty,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("Birthdate"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    controller: dateCtl,
                    decoration: textFormFieldDecoration,
                    //initialValue: birthDate,
                    onTap: () async {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      var date = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(1980),
                          lastDate: DateTime.now());
                      FocusScope.of(context).requestFocus(new FocusNode());
                      setState(() {
                        dateCtl.text = "${date.day}/${date.month}/${date.year}";
                      });
                    },
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),

                  //
                  Text("ID Number"),
                  SizedBox(
                    height: 4,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: textFormFieldDecoration,
                    autocorrect: false,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
              RaisedButton(
                color: Colors.blue,
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      // return object of type Dialog
                      return AlertDialog(
                        title: Text(
                          "Confirmation",
                          style: TextStyle(color: Colors.black),
                        ),
                        content: Text(
                          "Are the following details correct?",
                          style: TextStyle(color: Colors.black),
                        ),
                        actions: <Widget>[
                          // usually buttons at the bottom of the dialog
                          FlatButton(
                            child: Text(
                              "Confirm",
                              style: TextStyle(color: Colors.blue),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                              Router.navigator.pushNamed(
                                Router.sendingCredentialPage,
                                arguments: SendingCredentialPageArguments(
                                  credentialName: "Guardian ID",
                                ),
                              );
                            },
                          ),
                          FlatButton(
                            child: Text(
                              "Edit",
                              style: TextStyle(color: Colors.black),
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                color: Colors.white,
                onPressed: () {},
                child: Text(
                  "Cancel",
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
