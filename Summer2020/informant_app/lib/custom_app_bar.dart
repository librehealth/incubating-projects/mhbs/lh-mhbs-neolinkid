import 'package:flutter/material.dart';
import 'package:informant_app/util/strings.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.blue,
      leading: Padding(
        padding: EdgeInsets.only(left: 10.0),
        child: GestureDetector(
          onTap: () {},
          child: Image.asset(
            "assets/mhbs.png",
            width: 2.0,
            fit: BoxFit.contain,
          ),
        ),
      ),
      title: Text(
        appName,
      ),
      centerTitle: true,
      actions: <Widget>[],
    );
  }

  @override
  Size get preferredSize => Size(0.0, 56.0);
}
