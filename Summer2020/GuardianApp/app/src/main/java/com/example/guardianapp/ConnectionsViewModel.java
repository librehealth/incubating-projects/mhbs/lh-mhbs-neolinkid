package com.example.guardianapp;

import static com.example.guardianapp.lib.connection.ConnectionCreateResult.FAILURE;
import static com.example.guardianapp.lib.connection.ConnectionCreateResult.REDIRECT;
import static com.example.guardianapp.lib.connection.ConnectionCreateResult.SUCCESS;

import android.app.Application;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.webkit.URLUtil;

import com.example.guardianapp.db.Database;
import com.example.guardianapp.db.entity.Connection;
import com.example.guardianapp.lib.Connections;
import com.example.guardianapp.lib.connection.ConnectionCreateResult;
import com.example.guardianapp.lib.connection.QRConnection;
import com.example.guardianapp.lib.message.MessageState;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


public class ConnectionsViewModel extends AndroidViewModel {
    private final Database db;
    private static final String TAG = "ConnectionsViewModel";
    private MutableLiveData<List<Connection>> connections;

    public ConnectionsViewModel(@NonNull Application application) {
        super(application);
        db = Database.getInstance(application);
    }

    public LiveData<List<Connection>> getConnections() {
        if (connections == null) {
            connections = new MutableLiveData<>();
        }
        loadConnections();
        return connections;
    }

    public SingleLiveData<ConnectionCreateResult> newConnection(String invite) {
        SingleLiveData<ConnectionCreateResult> data = new SingleLiveData<>();
        createConnection(invite, data);
        return data;
    }

    private void loadConnections() {
        Log.d("ConnectionsViewModel","loadingConnections");
        Executors.newSingleThreadExecutor().execute(() -> {
            List<Connection> data = db.connectionDao().getAll();
            connections.postValue(data);
        });
    }

    private void createConnection(String invite, SingleLiveData<ConnectionCreateResult> liveData) {
        Log.d("createConnection:",invite+"!");
        Executors.newSingleThreadExecutor().execute(() -> {
            String parsedInvite = parseInvite(invite);
            Log.d("parsedInvite:",parsedInvite+"!");
            ConnDataHolder data = extractDataFromInvite(parsedInvite);
            List<String> serializedConns = db.connectionDao().getAllSerializedConnections();
            Log.d("serialized:",serializedConns.toString()+"!");
            Connections.verifyConnectionExists(parsedInvite, serializedConns)
                    .handle((exists, err) -> {
                        Log.d(TAG,"makeConnection:exists:"+exists);
                        Log.d(TAG,"makeConnection:err:"+err);
                        if (err != null) {
                            err.printStackTrace();
                            liveData.postValue(FAILURE);
                        } else {
                            if (exists) {
                                liveData.postValue(REDIRECT);
                            } else {
                                Connections.create(parsedInvite, new QRConnection())
                                        .handle((res, throwable) -> {
                                            Log.d(TAG,"create:res:"+res);
                                            Log.d(TAG,"create:throwable:"+throwable);
                                            if (res != null) {
                                                String serializedCon = Connections.awaitStatusChange(res, MessageState.ACCEPTED);
                                                Util.printLogs(TAG,"serializedConnection",serializedCon);
                                                Connection c = new Connection();
                                                c.name = Objects.requireNonNull(data).name;
                                                c.icon = data.logo;
                                                c.serialized = serializedCon;
                                                db.connectionDao().insertAll(c);
                                                //loadConnections();
                                            }
                                            Log.d(TAG,"after serialized content");
                                            if (throwable != null) {
                                                throwable.printStackTrace();
                                            }
                                            liveData.postValue(throwable == null ? SUCCESS : FAILURE);
                                            return res;
                                        });
                            }
                        }


                        return null;
                    });
        });
    }

    private String parseInvite(String invite) {
        if (URLUtil.isValidUrl(invite)) {
            Uri uri = Uri.parse(invite);
            Log.d("parseInviteMethod",uri.toString()+"!");
            String param = uri.getQueryParameter("c_i");
            if (param != null) {
                return new String(Base64.decode(param, Base64.NO_WRAP));
            } else {
                return "";
            }
        } else {
            return invite;
        }
    }

    private ConnDataHolder extractDataFromInvite(String invite) {
        try {
            JSONObject json = new JSONObject(invite);
            if (json.has("label")) {
                String label = json.getString("label");
                String logo = json.getString("profileUrl");
                return new ConnDataHolder(label, logo);
            }
            JSONObject data = json.optJSONObject("s");
            if (data != null) {
                return new ConnDataHolder(data.getString("n"), data.getString("l"));
            } else {
                // workaround in case details missing
                String sourceId = json.getString("id");
                return new ConnDataHolder(sourceId, null);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    static class ConnDataHolder {
        String name;
        String logo;

        public ConnDataHolder(String name, String logo) {
            this.name = name;
            this.logo = logo;
        }
    }
}
