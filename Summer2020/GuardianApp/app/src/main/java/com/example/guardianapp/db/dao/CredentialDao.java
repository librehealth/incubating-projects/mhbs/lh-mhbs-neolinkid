package com.example.guardianapp.db.dao;

import com.example.guardianapp.db.entity.Credential;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface CredentialDao {
    @Query("SELECT * FROM credential")
    List<Credential> getAll();

    @Query("SELECT * FROM credential WHERE id = :id")
    Credential getById(int id);

    @Insert
    void insertAll(Credential... credentials);

    @Query("SELECT EXISTS(SELECT * FROM credential WHERE (attributes = :attributes AND connection_id = :connectionId))")
    boolean checkOfferExists(String attributes, int connectionId);

    @Update
    void update(Credential connection);

    @Delete
    void delete(Credential connection);
}
