package com.example.guardianapp;

import android.content.Intent;
import android.os.Bundle;

import com.example.guardianapp.databinding.ActivityChildBinding;
import com.example.guardianapp.db.entity.Credential;
import com.example.guardianapp.interfaces.CredentialInterface;
import com.example.guardianapp.ui.home.Child;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import androidx.recyclerview.widget.LinearLayoutManager;

public class ChildActivity extends BaseActivity implements CredentialInterface {

    private ActivityChildBinding mChildBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChildBinding = ActivityChildBinding.inflate(getLayoutInflater());
        setContentView(mChildBinding.getRoot());

        mChildBinding.ibBack.setOnClickListener(v -> finish());
        mChildBinding.rvChild.setHasFixedSize(false);
        mChildBinding.rvChild.setLayoutManager(new LinearLayoutManager(this));

        if (SingletonClass.getInstance(getApplicationContext()).isInDemoMode()) {
            Child child = Objects.requireNonNull(getIntent().getExtras()).getParcelable(
                    getString(R.string.child));
            try {
                JSONObject jsonObject = new JSONObject(child.getChildCredential().getAttributes());
                mChildBinding.tvChildname.setText(
                        jsonObject.getString(getString(R.string.child_first_name)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ArrayList<Credential> credentialArrayList = new ArrayList<>();
            credentialArrayList.add(child.getChildCredential());
            credentialArrayList.addAll(child.getCredentials());
            CredentialAdapter credentialAdapter = new CredentialAdapter(credentialArrayList, this,
                    1);
            mChildBinding.rvChild.setAdapter(credentialAdapter);

        } else {

        }


    }

    public void goToCredential(Credential credential) {
        Intent intent = new Intent(this, CredentialActivity.class);
        intent.putExtra(getString(R.string.credential_offer), credential);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
        //overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_down);

    }
}