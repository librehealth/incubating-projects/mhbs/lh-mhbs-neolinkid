package com.example.guardianapp;

import android.util.Log;

public class Util {
    public static void printLogs(String TAG, String message, String actualLog){
        if(actualLog.length() > 4000) {
            int chunkCount = actualLog.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= actualLog.length()) {
                    Log.v(TAG, message+"::chunk " + i + " of " + chunkCount + "::" + actualLog.substring(4000 * i));
                } else {
                    Log.v(TAG, message+"::chunk " + i + " of " + chunkCount + "::" + actualLog.substring(4000 * i, max));
                }
            }
        } else {
            Log.d(TAG,message+"::"+actualLog);
        }
    }
}
