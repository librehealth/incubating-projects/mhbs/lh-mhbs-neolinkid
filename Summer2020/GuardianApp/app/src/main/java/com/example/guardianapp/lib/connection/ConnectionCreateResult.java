package com.example.guardianapp.lib.connection;

public enum ConnectionCreateResult {
    SUCCESS,
    REDIRECT,
    FAILURE
}
