package com.example.guardianapp;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    protected MainApplication mainApp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainApp = (MainApplication)this.getApplicationContext();
    }
    protected void onResume() {
        super.onResume();
        mainApp.setCurrentActivity(this);
    }
    protected void onPause() {
        clearReferences();
        super.onPause();
    }
    protected void onDestroy() {
        clearReferences();
        super.onDestroy();
    }

    private void clearReferences(){
        Activity currActivity = mainApp.getCurrentActivity();
        if (this.equals(currActivity)) {
            mainApp.setCurrentActivity(null);
        }
    }

    protected void printLogs(String TAG, String message, String actualLog){
        if(actualLog.length() > 4000) {
            int chunkCount = actualLog.length() / 4000;     // integer division
            for (int i = 0; i <= chunkCount; i++) {
                int max = 4000 * (i + 1);
                if (max >= actualLog.length()) {
                    Log.v(TAG, message+"::chunk " + i + " of " + chunkCount + "::" + actualLog.substring(4000 * i));
                } else {
                    Log.v(TAG, message+"::chunk " + i + " of " + chunkCount + "::" + actualLog.substring(4000 * i, max));
                }
            }
        } else {
            Log.d(TAG,message+"::"+actualLog);
        }
    }
}