package com.example.guardianapp.interfaces;

import com.example.guardianapp.db.entity.Credential;

public interface CredentialInterface {

    public void goToCredential(Credential credential);
}
